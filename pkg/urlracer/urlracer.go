package main

import (
	"fmt"
	"net/http"
	"time"
)

func Racer(u1, u2 string) (string, error) {
	return configurableRacer(u1, u2, 10*time.Second)
}

func configurableRacer(u1, u2 string, timeout time.Duration) (string, error) {
	select {
	case <-ping(u1):
		return u1, nil
	case <-ping(u2):
		return u2, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timed out waiting for %s and %s", u1, u2)
	}
}

func ping(url string) chan bool {
	ch := make(chan bool)
	go func() {
		http.Get(url)
		ch <- true
	}()
	return ch
}
