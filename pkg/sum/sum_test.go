package sum

import (
	"reflect"
	"testing"
)

func testSums(t *testing.T, got, expected []int) {
	t.Helper()
	if !reflect.DeepEqual(expected, got) {
		t.Errorf("expected %v , got %v", expected, got)
	}
}

func expect(t *testing.T, expected, got int, numbers []int) {
	t.Helper()
	if expected != got {
		t.Errorf("expected %d got %d given %v", expected, got, numbers)
	}

}
func TestSum(t *testing.T) {
	t.Run("Sum of arbetrary sized collection", func(t *testing.T) {
		numbers := []int{1, 2, 3}

		got := Sum(numbers)
		expected := 6

		expect(t, expected, got, numbers)

	})

	t.Run("Sum of an empty Slice", func(t *testing.T) {
		numbers := []int{}

		got := Sum(numbers)
		expected := 0

		expect(t, expected, got, numbers)
	})
}

func TestSumAllTails(t *testing.T) {
	t.Run("tail sum of some defined slices", func(t *testing.T) {
		got := SumAllTails([]int{1, 2, 3}, []int{4, 5, 6})
		expected := []int{5, 11}

		testSums(t, got, expected)

	})

	t.Run("tail sum with an emplty array", func(t *testing.T) {
		got := SumAllTails([]int{1, 2, 3}, []int{})
		expected := []int{5, 0}

		testSums(t, got, expected)

	})
}
