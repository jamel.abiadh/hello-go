package sum

func Sum(nums []int) int {
	sum := 0
	for _, num := range nums {
		sum += num
	}
	return sum
}

func sumAll(nums ...[]int) []int {
	sumAll := []int{}
	for _, num := range nums {
		sumAll = append(sumAll, Sum(num))
	}

	return sumAll
}

func SumAllTails(nums ...[]int) []int {
	var tails [][]int
	for _, num := range nums {
		if len(num) == 0 {
			tails = append(tails, []int{0})
		} else {
			tails = append(tails, num[1:])
		}
	}
	return sumAll(tails...)
}
