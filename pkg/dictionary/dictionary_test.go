package main

import "testing"

func TestSearch(t *testing.T) {
	dictionary := Dictionary{"test": "this is just a test"}

	t.Run("known world", func(t *testing.T) {
		got, _ := dictionary.Search("test")
		want := "this is just a test"

		assertString(t, got, want)
	})

	t.Run("unknowen word", func(t *testing.T) {
		_, err := dictionary.Search("unknowen")

		assertErrNotFound(t, err)
	})
}

func TestAdd(t *testing.T) {
	t.Run("new word", func(t *testing.T) {
		dictionary := Dictionary{}
		word := "test"
		definition := "this is just a test"

		dictionary.Add(word, definition)

		assertDefinition(t, dictionary, word, definition)
	})

	t.Run("existing word", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"

		dictionary := Dictionary{word: definition}

		err := dictionary.Add(word, definition)
		assertErrWordExists(t, dictionary, word, definition, err)
	})
}

func TestUpdate(t *testing.T) {
	t.Run("existing word", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"
		newDefinition := "new definition"
		dictionary := Dictionary{word: definition}
		dictionary.Update(word, newDefinition)

		assertDefinition(t, dictionary, word, newDefinition)
	})

	t.Run("new word", func(t *testing.T) {
		word := "test"
		newDefinition := "new definition"
		dictionary := Dictionary{}
		err := dictionary.Update(word, newDefinition)

		assertErrWordDoesNotExist(t, err)
	})
}

func TestDelete(t *testing.T) {
	word := "test"
	dictionary := Dictionary{word: "test definition"}

	dictionary.Delete(word)

	_, err := dictionary.Search(word)
	if err != ErrNotFound {
		t.Errorf("Expected %q to be deleted", word)
	}
}

func assertDefinition(t *testing.T, dictionary Dictionary, word, definition string) {
	t.Helper()

	got, err := dictionary.Search(word)
	if err != nil {
		t.Fatal("Should find the added world", err)
	}

	if definition != got {
		t.Errorf("got %q want %q", got, definition)
	}
}

func assertString(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q given, %q", got, want, "test")
	}
}

func assertError(t *testing.T, err, wantErr error) {
	t.Helper()
	if err != wantErr {
		t.Fatalf("Experted %s, got %s", wantErr, err)
	}
}

func assertErrNotFound(t *testing.T, err error) {
	assertError(t, err, ErrNotFound)
}

func assertErrWordDoesNotExist(t *testing.T, err error) {
	assertError(t, err, ErrWordDoesNotExist)
}

func assertErrWordExists(t *testing.T, dictionary Dictionary, word, definition string, err error) {
	assertError(t, err, ErrWordExists)
	assertDefinition(t, dictionary, word, definition)
}
