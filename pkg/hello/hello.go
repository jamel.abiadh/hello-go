package main

import "fmt"

var greeting = map[string]string{
	"en": "Hello",
	"sp": "Hola",
	"fr": "Salut",
}

//Hello says hello
func Hello(n, ln string) string {
	if ln == "" {
		ln = "en"
	}
	if n == "" {
		n = "World"
	}
	return greeting[ln] + ", " + n
}

func main() {
	fmt.Println(Hello("jabiadh", "sp"))
}
