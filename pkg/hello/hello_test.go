package main

import "testing"

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}
	t.Run("Say hello to poeple", func(t *testing.T) {
		got := Hello("jabiadh", "")
		want := "Hello, jabiadh"

		assertCorrectMessage(t, got, want)
	})

	t.Run("Say 'Hello, World' when an empty string is passed", func(t *testing.T) {
		got := Hello("", "")
		want := "Hello, World"

		assertCorrectMessage(t, got, want)
	})

	t.Run("in Spanish", func(t *testing.T) {
		got := Hello("jabiadh", "sp")
		want := "Hola, jabiadh"
		assertCorrectMessage(t, got, want)
	})

	t.Run("in French", func(t *testing.T) {
		got := Hello("jabiadh", "fr")
		want := "Salut, jabiadh"
		assertCorrectMessage(t, got, want)
	})
}
