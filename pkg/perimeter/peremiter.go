package perimeter

import "math"

type Rectangle struct {
	Hight float64
	Width float64
}

type Circle struct {
	Radius float64
}

type Shape interface {
	Area() float64
}

type Triangle struct {
	Base, Hight float64
}

func (t Triangle) Area() float64 {
	return t.Base * t.Hight / 2
}

func (r Rectangle) Perimeter() float64 {
	return 2 * (r.Hight + r.Width)
}

func (r Rectangle) Area() float64 {
	return r.Hight * r.Width
}

func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}
