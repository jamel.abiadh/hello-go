package iteration

func Repeat(s string, n int) string {
	rs := ""
	for i := 0; i < n; i++ {
		rs += s
	}
	return rs

}
